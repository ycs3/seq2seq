import torch
import torch.nn as nn
from torch import optim
import torch.nn.functional as F

import string
import random
import time
import pickle

from lang import Lang, word2vec, vec2word

random.seed()

device = torch.device("cuda")

def shuffle_and_sample(pairs, num):
    pairs = [random.choice(pairs) for _ in range(num)]
    en_data, fr_data = list(zip(*pairs))
    return en_data, fr_data

def torch_data(lang, data, device):
    return [
        torch.tensor([
            [v] for v in [lang.idx[lang.SOS]] + sent + [lang.idx[lang.EOS]]
        ], device=device) for sent in data
    ]

class EncoderRNN(nn.Module):
    def __init__(self, input_size, hidden_size):
        super(EncoderRNN, self).__init__()
        self.hidden_size = hidden_size
        self.embedding = nn.Embedding(input_size, hidden_size)
        self.gru = nn.GRU(hidden_size, hidden_size)
        self.out = nn.Linear(hidden_size, input_size)
        self.softmax = nn.LogSoftmax(dim=1)

    def forward(self, input, hidden):
        embedded = self.embedding(input).view(1, 1, -1)
        output = embedded
        output, hidden = self.gru(output, hidden)
        output = self.softmax(self.out(output[0]))
        return output, hidden

class DecoderRNN(nn.Module):
    def __init__(self, hidden_size, output_size):
        super(DecoderRNN, self).__init__()
        self.hidden_size = hidden_size
        self.embedding = nn.Embedding(output_size, hidden_size)
        self.gru = nn.GRU(hidden_size, hidden_size)
        self.out = nn.Linear(hidden_size, output_size)
        self.softmax = nn.LogSoftmax(dim=1)

    def forward(self, input, hidden):
        output = self.embedding(input).view(1, 1, -1)
        output = F.relu(output)
        output, hidden = self.gru(output, hidden)
        output = self.softmax(self.out(output[0]))
        return output, hidden

def initHidden(hidden_size, device):
    return torch.zeros(1, 1, hidden_size, device=device)

def evaluate(input_tensor, target_tensor, input_lang, target_lang, hidden_size, device):
    with torch.no_grad():
        hidden = initHidden(hidden_size, device)

        fr_words = []
        encoder_input = input_tensor[0]
        for ei in range(input_tensor.size(0)):
            encoder_output, hidden = enc(input_tensor[ei], hidden)
            topv, topi = encoder_output.data.topk(1)
            word = fr.ridx[topi.item()]
            fr_words.append(word)

        words = []
        decoder_input = target_tensor[0]
        for di in range(1, target_tensor.size(0)):
            decoder_output, hidden = dec(decoder_input, hidden)
            topv, topi = decoder_output.data.topk(1)
            word = en.ridx[topi.item()]
            words.append(word)
            decoder_input = topi.squeeze().detach()

        print(vec2word(input_lang, input_tensor, str))
        print("^ " + " ".join(fr_words))
        print("-----")

        print(vec2word(target_lang, target_tensor, str))
        print("^ " + " ".join(words))
        print()

if __name__ == '__main__':
    with open("eng-fra-lang.pkl", "rb") as fp:
        en, fr, pairs = pickle.load(fp)

    n_iters = 300000

    en_data, fr_data = shuffle_and_sample(pairs, n_iters)

    hidden_size = 256
    learning_rate = .01

    fr_torch = torch_data(fr, fr_data, device)
    en_torch = torch_data(en, en_data, device)

    print("data loaded.")
    print(vec2word(fr, fr_torch[5000], str), "|", vec2word(en, en_torch[5000], str))
    print()

    fr_emb = nn.Embedding(fr.size, hidden_size)
    en_emb = nn.Embedding(en.size, hidden_size)

    enc = EncoderRNN(fr.size, hidden_size).to(device)
    dec = DecoderRNN(hidden_size, en.size).to(device)

    enc_optim = optim.SGD(enc.parameters(), lr=learning_rate)
    dec_optim = optim.SGD(dec.parameters(), lr=learning_rate)

    criterion = nn.NLLLoss()

    total_loss = 0
    interval_loss = 0

    for idx in range(1, n_iters+1):
        input_tensor = fr_torch[idx-1]
        target_tensor = en_torch[idx-1]

        enc_optim.zero_grad()
        dec_optim.zero_grad()

        hidden = initHidden(hidden_size, device)

        loss = 0
        for ei in range(input_tensor.size(0)):
            encoder_output, hidden = enc(input_tensor[ei], hidden)
            if ei == input_tensor.size(0) - 1:
                loss += criterion(encoder_output, target_tensor[0])
            else:
                loss += criterion(encoder_output, input_tensor[ei+1])

        decoder_input = target_tensor[0]
        for di in range(1, target_tensor.size(0)):
            decoder_output, hidden = dec(decoder_input, hidden)
            loss += criterion(decoder_output, target_tensor[di])
            decoder_input = target_tensor[di]

        loss.backward()

        enc_optim.step()
        dec_optim.step()

        sample_loss = loss.item() / target_tensor.size(0)
        interval_loss += sample_loss
        total_loss += sample_loss

        if idx % 500 == 0:
            print("%6d" % idx, "%.4f" % (interval_loss/500))
            interval_loss = 0

            evaluate(fr_torch[4000], en_torch[4000], fr, en, hidden_size, device)
            evaluate(fr_torch[6000], en_torch[6000], fr, en, hidden_size, device)

        if idx % 10000 == 0:
            torch.save({
                "enc_state_dict": enc.state_dict(),
                "dec_state_dict": dec.state_dict(),
                "enc_optim_state_dict": enc_optim.state_dict(),
                "dec_optim_state_dict": dec_optim.state_dict(),
            }, "./output/model_%06d.pth" % idx)

    print("%.4f" % (total_loss / n_iters))

if __name__ == '__main__':
    main()
