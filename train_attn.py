import torch
import torch.nn as nn
from torch import optim
import torch.nn.functional as F

import string
import random
import time
import pickle
import numpy as np

from lang import Lang, word2vec, vec2word,\
                 shuffle_and_sample, torch_data, reverse_tensor
#from labplot import lplot

random.seed()

device = torch.device("cuda")

class EncDecRNN(nn.Module):
    def __init__(self, vocab_size, hidden_size):
        super(EncDecRNN, self).__init__()
        self.hidden_size = hidden_size
        self.embedding = nn.Embedding(vocab_size, hidden_size)

        # input:    accepts as input (seq_len, batch, input_size)
        #           initial hidden input (layers, batch, hidden_size) default 0
        # output:   outputs (seq_len, batch, hidden_size)
        #           hidden (layers, batch, hidden_size)
        # gru args: input_size, hidden_size
        self.gru = nn.GRU(hidden_size, hidden_size)

        # input:  (1, hidden_size=256)
        # output: (1, vocab_size=en:3354)
        self.out = nn.Linear(hidden_size, vocab_size)
        self.softmax = nn.LogSoftmax(dim=1)

    def forward(self, vocab, hidden):
        # input: vocab: [1] -> (seq_len=1, batch=1, 1-or really en:3354)
        # output: output: (seq_len=1, batch=1, hidden_size=256)
        output = self.embedding(vocab).view(1, 1, -1)

        # [input]
        # output: (1, 1, input_size=256), hidden: (1, 1, 256)
        # [output]
        # output: (1, 1, hidden_size=256), hidden: (1, 1, 256)
        output, hidden = self.gru(output, hidden)

        # [input]
        # output[0]: (1, hidden_size=256)
        # [output]
        # output: (1, en:3354)
        output = self.out(output[0])

        # [input] (1, 3354), [output] (1, 3354)
        output = self.softmax(output)
        return output, hidden


class EncoderRNN(nn.Module):
    def __init__(self, vocab_size, hidden_size):
        super(EncoderRNN, self).__init__()
        self.hidden_size = hidden_size
        self.embedding = nn.Embedding(vocab_size, hidden_size)
        # input:    accepts as input (seq_len, batch, input_size)
        #           initial hidden input (layers, batch, hidden_size) default 0
        # output:   outputs (seq_len, batch, hidden_size)
        #           hidden (layers, batch, hidden_size)
        # gru args: input_size, hidden_size
        self.gru = nn.GRU(hidden_size, hidden_size)

    def forward(self, vocab, hidden):
        # the output is (layers=1, batch=1, input_size=256)
        output = self.embedding(vocab).view(1, 1, -1)

        # input:  output (seq_len=1, batch=1, input_size=256)
        #         hidden (layers=1, batch=1, hidden_size=256)
        # output: output (seq_len=1, batch=1, hidden_size=256)
        #         hidden (layers=1, batch=1, hidden_size=256)
        output, hidden = self.gru(output, hidden)

        return output, hidden

MAX_LENGTH=13

class AttnDecoderRNN(nn.Module):
    def __init__(self, vocab_size, hidden_size, dropout_p=.1, max_length=MAX_LENGTH):
        super(AttnDecoderRNN, self).__init__()
        self.vocab_size = vocab_size
        self.hidden_size = hidden_size
        self.dropout_p = dropout_p
        self.max_length = max_length

        # embedding: [12] -> (256)
        self.embedding = nn.Embedding(self.vocab_size, self.hidden_size)
        self.attn = nn.Linear(self.hidden_size * 2, self.max_length)

        self.attn_combine = nn.Linear(self.hidden_size * 2, self.hidden_size)

        self.dropout = nn.Dropout(self.dropout_p)
        self.gru = nn.GRU(self.hidden_size, self.hidden_size)
        self.out = nn.Linear(self.hidden_size, self.vocab_size)

    def forward(self, vocab, hidden, encoder_outputs):
        # outputs (256) -view-> (1, 1, 256)
        embedded = self.embedding(vocab).view(1, 1, -1)

        # (1, 1, 256) -> (1, 1, 256), randomly zeros
        embedded = self.dropout(embedded)

        # (1, 1, 256), (1, 1, 256) -> (1, 512)
        attn_cat = torch.cat((embedded[0], hidden[0]), 1)
        # (1, 512) -> (1, max_length=13)
        attn_ = self.attn(attn_cat)
        # softmax over (1, 13)
        attn_weights = F.softmax(attn_, dim=1)
        # attn_weights=(1, 1, 13), encoder_outputs=(1, 13, 256)
        # output will be (1, 1, 256)
        attn_applied = torch.bmm(attn_weights.unsqueeze(0),
                                 encoder_outputs.unsqueeze(0))

        # embedded=(1, 1, 256), attn_applied=(1, 1, 256)
        # embedded[0]=(1, 256), attn_applied[0]=(1, 256)
        # (1, 512)
        output = torch.cat((embedded[0], attn_applied[0]), 1)

        # (1, 512) -> (1, 256) -> (1, 1, 256)
        output = self.attn_combine(output).unsqueeze(0)

        # (1, 1, 256) -> (1, 1, 256)
        output = F.relu(output)

        # input: (1, 1, 256), hidden: (1, 1, 256)

        # input:  output (seq_len=1, batch=1, input_size=256)
        #         hidden (layers=1, batch=1, hidden_size=256)
        # output: output (seq_len=1, batch=1, hidden_size=256)
        #         hidden (layers=1, batch=1, hidden_size=256)
        output, hidden = self.gru(output, hidden)

        # [out]
        # (seq_len=1, batch=1, hidden_size=256) -> output[0]=(1, 256)
        # -> (1, vocab_size=en:3354)
        # output: (1, en:3354)
        output = F.log_softmax(self.out(output[0]), dim=1)

        return output, hidden, attn_weights

def initHidden(hidden_size, device):
    return torch.zeros(1, 1, hidden_size, device=device)

def train(idx, input_lang, input_tensor, hidden_size, device, enc, dec, enc_optim, dec_optim, criterion):
    hidden = initHidden(hidden_size, device)
    loss = 0

    reversed_tensor = reverse_tensor(input_tensor)

    # (13, 256)
    encoder_outputs = torch.zeros(MAX_LENGTH, hidden_size, device=device)

    encoder_input = input_tensor[0]
    for ei in range(1, input_tensor.size(0)):
        #encoder_output, hidden = enc(input_tensor[ei], hidden)
        encoder_output, hidden = enc(encoder_input, hidden)
        #loss += criterion(encoder_output, input_tensor[ei])

        encoder_input = input_tensor[ei]
        encoder_outputs[ei] += encoder_output[0, 0]

        #topv, topi = encoder_output.data.topk(1)
        #encoder_input = topi.squeeze().detach()

    decoder_input = reversed_tensor[0]
    for di in range(1, reversed_tensor.size(0)):
        decoder_output, hidden, decoder_attention = dec(decoder_input, hidden, encoder_outputs)
        loss += criterion(decoder_output, reversed_tensor[di])

        decoder_input = reversed_tensor[di]

        #topv, topi = decoder_output.data.topk(1)
        #decoder_input = topi.squeeze().detach()

    loss.backward()
    enc_optim.step()
    dec_optim.step()

    #sample_loss = loss.item() / ((input_tensor.size(0)-1)+(reversed_tensor.size(0)-1))
    sample_loss = loss.item() / (reversed_tensor.size(0)-1)
    return sample_loss

def eval(idx, input_lang, input_tensor, hidden_size, device, enc, dec, criterion):
    hidden_saved = []
    with torch.no_grad():
        hidden = initHidden(hidden_size, device)
        en_words = []
        losses = []

        reversed_tensor = reverse_tensor(input_tensor)

        encoder_outputs = torch.zeros(MAX_LENGTH, hidden_size, device=device)

        encoder_input = input_tensor[0]
        for ei in range(1, input_tensor.size(0)):
            #_, hidden = enc(input_tensor[ei], hidden)
            encoder_output, hidden = enc(encoder_input, hidden)
            #losses.append("%.4f" % criterion(encoder_output, input_tensor[ei]).item())
            #topv, topi = encoder_output.data.topk(1)
            #word = input_lang.ridx[topi.item()]
            #en_words.append(word)
            encoder_input = input_tensor[ei]

        en_words.append("|")
        losses.append("|")

        decoder_attentions = torch.zeros(MAX_LENGTH, MAX_LENGTH)

        decoder_input = reversed_tensor[0]
        for di in range(1, reversed_tensor.size(0)):
            hidden_saved.append(hidden)
            decoder_output, hidden, decoder_attention = dec(decoder_input, hidden, encoder_outputs)
            decoder_attentions[di] = decoder_attention.data
            losses.append("%.4f" % criterion(decoder_output, reversed_tensor[di]).item())
            topv, topi = decoder_output.data.topk(1)
            word = input_lang.ridx[topi.item()]
            en_words.append(word)
            decoder_input = topi.squeeze().detach()

    orig_input_ = vec2word(input_lang, input_tensor, str)[2:].split(" ")
    orig_output_ = vec2word(input_lang, reversed_tensor, str)[2:].split(" ")
    orig_input = ["%7s" % k for k in orig_input_]
    orig_output = ["%7s" % k for k in orig_output_]

    #print(" ".join(orig_input), "%7s" % "|", " ".join(orig_output))
    print("%7s" % "|", " ".join(orig_output))
    print(" ".join(["%7s" % k for k in en_words]))
    print(" ".join(["%7s" % k for k in losses]))
    print()

    return " ".join(orig_input), hidden_saved, decoder_attentions


def main():
    with open("eng-fra-lang.pkl", "rb") as fp:
        en, fr, pairs = pickle.load(fp)

    n_iters = 75000
    en_data, fr_data = shuffle_and_sample(pairs, n_iters)
    hidden_size = 256
    learning_rate = .01

    en_torch = torch_data(en, en_data, device)
    print("data loaded.")
    print(vec2word(en, en_torch[5000], str))
    print()

    en_emb = nn.Embedding(en.size, hidden_size)
    #enc = EncDecRNN(en.size, hidden_size).to(device)
    #dec = EncDecRNN(en.size, hidden_size).to(device)
    enc = EncoderRNN(en.size, hidden_size).to(device)
    dec = AttnDecoderRNN(en.size, hidden_size).to(device)
    enc_optim = optim.SGD(enc.parameters(), lr=learning_rate)
    dec_optim = optim.SGD(dec.parameters(), lr=learning_rate)
    criterion = nn.NLLLoss()

    total_loss = 0
    interval_loss = 0

    for idx in range(1, n_iters + 1):
        input_tensor = en_torch[idx - 1]
        enc_optim.zero_grad()
        dec_optim.zero_grad()

        sample_loss = train(idx, en, input_tensor, hidden_size, device, enc, dec, enc_optim, dec_optim, criterion)

        interval_loss += sample_loss
        total_loss += sample_loss

        if idx % 500 == 0:
            print("%6d" % idx, "%.4f" % (interval_loss/500))
            #lplot.point(epoch=idx, loss=interval_loss/500, refresh=5)
            interval_loss = 0

            attn = {}
            for k in [2000, 4000, 6000, 8000]:
                sent, hidden_saved, attentions = eval(idx, en, en_torch[k], hidden_size, device, enc, dec, criterion)
                attn[k] = (attentions, hidden_saved)
            with open("./output/attn_fwd_%06d.pkl" % idx, "wb") as fp:
                pickle.dump(attn, fp)
            

        if idx % 10000 == 0:
            torch.save({
                "enc_state_dict": enc.state_dict(),
                "enc_optim_state_dict": enc_optim.state_dict(),
            }, "./output/enc_attn_model_%06d.pth" % idx)

    print("%.4f" % (total_loss / n_iters))

if __name__ == '__main__':
    main()
