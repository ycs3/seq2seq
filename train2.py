import torch
import torch.nn as nn
from torch import optim
import torch.nn.functional as F

import string
import random
import time
import pickle
import numpy as np

from lang import Lang, word2vec, vec2word,\
                 shuffle_and_sample, torch_data, reverse_tensor
#from labplot import lplot

random.seed()

device = torch.device("cuda")

class EncoderRNN(nn.Module):
    def __init__(self, input_size, hidden_size):
        super(EncoderRNN, self).__init__()
        self.hidden_size = hidden_size
        self.embedding = nn.Embedding(input_size, hidden_size)
        self.gru = nn.GRU(hidden_size, hidden_size)
        self.out = nn.Linear(hidden_size, input_size)
        self.softmax = nn.LogSoftmax(dim=1)

    def forward(self, input, hidden):
        embedded = self.embedding(input).view(1, 1, -1)
        output = embedded
        output, hidden = self.gru(output, hidden)
        output = self.softmax(self.out(output[0]))
        return output, hidden

class DecoderRNN(nn.Module):
    def __init__(self, hidden_size, output_size):
        super(DecoderRNN, self).__init__()
        self.hidden_size = hidden_size
        self.embedding = nn.Embedding(output_size, hidden_size)
        self.gru = nn.GRU(hidden_size, hidden_size)
        self.out = nn.Linear(hidden_size, output_size)
        self.softmax = nn.LogSoftmax(dim=1)

    def forward(self, input, hidden):
        output = self.embedding(input).view(1, 1, -1)
        output = F.relu(output)
        output, hidden = self.gru(output, hidden)
        output = self.softmax(self.out(output[0]))
        return output, hidden

class EncDecRNN(nn.Module):
    def __init__(self, vocab_size, hidden_size):
        super(EncDecRNN, self).__init__()
        self.hidden_size = hidden_size
        self.embedding = nn.Embedding(output_size, hidden_size)
        self.gru = nn.GRU(hidden_size, hidden_size)
        self.out = nn.Linear(hidden_size, vocab_size)
        self.softmax = nn.LogSoftmax(dim=1)

    def forward(self, vocab, hidden):
        output = self.embedding(vocab).view(1, 1, -1)
        output, hidden = self.gru(output, hidden)
        output = self.softmax(self.out(output[0]))
        return output, hidden


def initHidden(hidden_size, device):
    return torch.zeros(1, 1, hidden_size, device=device)

def train(input_lang, input_tensor, hidden_size, device, enc, dec, enc_optim, dec_optim, criterion):
    hidden = initHidden(hidden_size, device)
    loss = 0

    reversed_tensor = reverse_tensor(input_tensor)

    for ei in range(input_tensor.size(0)):
        encoder_output, hidden = enc(input_tensor[ei], hidden)

    decoder_input = reversed_tensor[0]
    for di in range(1, reversed_tensor.size(0)):
        decoder_output, hidden = dec(decoder_input, hidden)
        loss += criterion(decoder_output, reversed_tensor[di])

        topv, topi = decoder_output.data.topk(1)
        decoder_input = topi.squeeze().detach()

    loss.backward()
    enc_optim.step()
    dec_optim.step()

    sample_loss = loss.item() / reversed_tensor.size(0)
    return sample_loss

def eval(input_lang, input_tensor, hidden_size, device, enc, dec, criterion):
    with torch.no_grad():
        hidden = initHidden(hidden_size, device)
        en_words = []
        losses = []

        reversed_tensor = reverse_tensor(input_tensor)
        for ei in range(input_tensor.size(0)):
            _, hidden = enc(input_tensor[ei], hidden)

        decoder_input = reversed_tensor[0]
        for di in range(1, reversed_tensor.size(0)):
            decoder_output, hidden = dec(decoder_input, hidden)
            losses.append("%.4f" % criterion(decoder_output, reversed_tensor[di]).item())
            topv, topi = decoder_output.data.topk(1)
            word = input_lang.ridx[topi.item()]
            en_words.append(word)
            decoder_input = topi.squeeze().detach()

    print(vec2word(input_lang, reversed_tensor, str))
    print(" ".join(en_words))
    print(" ".join(losses))
    print()

def main():
    with open("eng-fra-lang.pkl", "rb") as fp:
        en, fr, pairs = pickle.load(fp)

    n_iters = 75000
    en_data, fr_data = shuffle_and_sample(pairs, n_iters)
    hidden_size = 256
    learning_rate = .01

    en_torch = torch_data(en, en_data, device)
    print("data loaded.")
    print(vec2word(en, en_torch[5000], str))
    print()

    en_emb = nn.Embedding(en.size, hidden_size)
    enc = EncoderRNN(en.size, hidden_size).to(device)
    dec = DecoderRNN(hidden_size, en.size).to(device)
    #enc = EncDecRNN(en.size, hidden_size).to(device)
    #dec = EncDecRNN(en.size, hidden_size).to(device)
    enc_optim = optim.SGD(enc.parameters(), lr=learning_rate)
    dec_optim = optim.SGD(dec.parameters(), lr=learning_rate)
    criterion = nn.NLLLoss()

    total_loss = 0
    interval_loss = 0

    for idx in range(1, n_iters + 1):
        input_tensor = en_torch[idx - 1]
        enc_optim.zero_grad()
        dec_optim.zero_grad()

        sample_loss = train(en, input_tensor, hidden_size, device, enc, dec, enc_optim, dec_optim, criterion)

        interval_loss += sample_loss
        total_loss += sample_loss

        if idx % 500 == 0:
            print("%6d" % idx, "%.4f" % (interval_loss/500))
            #lplot.point(epoch=idx, loss=interval_loss/500, refresh=5)
            interval_loss = 0

            for k in [2000, 4000, 6000, 8000]:
                eval(en, en_torch[k], hidden_size, device, enc, dec, criterion)

        if idx % 10000 == 0:
            torch.save({
                "enc_state_dict": enc.state_dict(),
                "enc_optim_state_dict": enc_optim.state_dict(),
            }, "./output/enc_model_%06d.pth" % idx)

    print("%.4f" % (total_loss / n_iters))

if __name__ == '__main__':
    main()
