import string
import random
import time
import pickle
import torch
import numpy as np

random.seed(0)

eng_prefixes = {
    "i am", "i m", "im",
    "he is", "he s", "hes",
    "you are", "you re", "youre",
    "we are", "we re", "were",
    "they are", "they re", "theyre",
}

class Lang:
    def __init__(self):
        self.idx = {}
        self.ridx = {}
        self.size = 0

        self.SOS = "^"
        self.EOS = "$"

        self.add_vocab(self.SOS)
        self.add_vocab(self.EOS)

    def add_vocab(self, word):
        if word in self.idx.keys():
            return
        self.idx[word] = self.size
        self.ridx[self.size] = word
        self.size += 1

def remove_punctuation(s):
    return s.translate(str.maketrans('', '', string.punctuation))

def vectorize_text(text):
    sent = remove_punctuation(text)
    words = [w.lower() for w in sent.split()]
    return words

def preprocess_text_line(line):
    sent1, sent2 = str(line.strip(), "utf-8").split("\t")
    words1 = vectorize_text(sent1)
    words2 = vectorize_text(sent2)
    return words1, words2

def filter_en_sentence(words):
    if len(words) > 10:
        return True
    if words[0] in eng_prefixes:
        return False
    if " ".join(words[:2]) in eng_prefixes:
        return False
    return True

def read_translation_file(filename):
    with open(filename, "rb") as fp:
        lines = fp.readlines()

    en = Lang()
    fr = Lang()

    en_data = []
    fr_data = []

    for line in lines:
        en_words, fr_words = preprocess_text_line(line)

        if filter_en_sentence(en_words):
            continue
        if len(fr_words) > 10:
            continue

        for w in en_words:
            en.add_vocab(w)
        for w in fr_words:
            fr.add_vocab(w)

        en_data.append([en.idx[word] for word in en_words])
        fr_data.append([fr.idx[word] for word in fr_words])

    pairs = list(zip(en_data, fr_data))

    return en, fr, pairs

def word2vec(lang, words):
    return [lang.idx[word] for word in words]

def vec2word(lang, vec, conv=None):
    if isinstance(vec[0], int):
        v = [lang.ridx[int(v)] for v in vec]
    else:
        v = [lang.ridx[int(v[0])] for v in vec]
    if conv == str:
        return " ".join(v)
    return v

def shuffle_and_sample(pairs, num):
    pairs = [random.choice(pairs) for _ in range(num)]
    en_data, fr_data = list(zip(*pairs))
    return en_data, fr_data

def torch_data(lang, data, device):
    return [
        torch.tensor([
            [v] for v in [lang.idx[lang.SOS]] + sent + [lang.idx[lang.EOS]]
        ], device=device) for sent in data
    ]

def reverse_tensor(input_tensor):
    return torch.from_numpy(np.flip(input_tensor.cpu().numpy(), 0).copy()).cuda()

if __name__ == '__main__':
    en, fr, pairs = read_translation_file("eng-fra.txt")

    with open("eng-fra-lang.pkl", "wb") as fp:
        pickle.dump([en, fr, pairs], fp)
